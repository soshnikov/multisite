# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.http import Http404

from pages.models import Page, Site, MenuItem


def index(request):
    host = request.META['HTTP_HOST']
    site = Site.objects.get(address=host)
    template_dir = site.slug
    page = Page.objects.filter(site=site, index=True).first()
    if page is None:
        page = Page.objects.filter(site=site).first()
    if page is None:
        raise Http404
    menu = MenuItem.objects.filter(site=site).order_by('order').all()
    data = {
        'site': site,
        'page': page,
        'menu': menu,
        'slug': '',
    }
    return render_to_response('%s/template.html' % template_dir, data)

def page(request, slug):
    host = request.META['HTTP_HOST']
    site = Site.objects.get(address=host)
    template_dir = site.slug
    try:
        page = Page.objects.get(slug=slug)
    except Exception:
        raise Http404

    menu = MenuItem.objects.filter(site=site).order_by('order').all()
    data = {
        'site': site,
        'page': page,
        'menu': menu,
        'slug': slug,
    }
    return render_to_response('%s/template.html' % template_dir, data)