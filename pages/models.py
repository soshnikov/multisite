# -*- coding: utf-8 -*-

from django.db import models
from redactor.fields import RedactorField


class Site(models.Model):
    """
    Имя сайта для мультисайта
    """

    name = models.CharField(u'Название сайта', max_length=100, null=False, blank=False, unique=True)
    address = models.CharField(u'Адрес сайта', max_length=150, null=False, blank=False, unique=True)
    slug = models.CharField(u'Каталог шаблонов', max_length=50, null=False, blank=False, unique=True)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()

    class Meta:
        verbose_name = u'сайт'
        verbose_name_plural = u'сайты'
        ordering = ['name']


class Page(models.Model):
    """
    Страница сайта
    """

    name = models.CharField(u'Название', max_length=50, null=False, blank=False, unique=True)
    title = models.CharField(u'Заголовок', max_length=100, null=False, blank=True)
    slug = models.CharField(u'URL-имя', max_length=100, null=False, blank=False)
    site = models.ForeignKey(Site, verbose_name=u'Сайт', null=False, blank=False)
    index = models.BooleanField(u'Главная страница сайта', null=False, default=False)
    text = RedactorField(u'Текст статьи', null=False, blank=True)
    meta_keywords = models.TextField(u'meta keywords', null=False, default=u'')
    meta_description = models.TextField(u'meta description', null=False, default=u'')
    counters_js = models.TextField(u'Коды счетчиков', null=False, default=u'')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()

    class Meta:
        verbose_name = u'статья'
        verbose_name_plural = u'статьи'
        ordering = ['site', 'index', 'name']


class MenuItem(models.Model):
    """
    Пункт главного меню сайта
    """

    name = models.CharField(u'Название', max_length=100, null=False, blank=False)
    page = models.ForeignKey(Page, verbose_name=u'Страница', null=True, blank=True)
    url = models.CharField(u'Ссылка', max_length=200, null=False, blank=True)
    order = models.IntegerField(u'Сортировка', null=False, default=0)
    site = models.ForeignKey(Site, null=False)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()

    class Meta:
        ordering = ('order',)
