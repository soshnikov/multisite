# -*- coding: utf-8 -*-

from django.contrib import admin

from pages.models import Site, Page, MenuItem
from adminsortable.admin import SortableInlineAdminMixin

class MenuItemAdmin(SortableInlineAdminMixin, admin.TabularInline):
    model = MenuItem


class SiteAdmin(admin.ModelAdmin):
    list_display = ['name', 'address', 'slug']
    inlines = [MenuItemAdmin]

    def change_view(self, request, object_id, extra_context=None):
        def formfield_for_foreignkey(self, db_field, request, **kwargs):
            if db_field.name == 'page':
                kwargs['queryset'] = Page.objects.filter(site__id=object_id)
            return super(MenuItemAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

        MenuItemAdmin.formfield_for_foreignkey = formfield_for_foreignkey
        self.inline_instances = [MenuItemAdmin(self.model, self.admin_site)]

        return super(SiteAdmin, self).change_view(request, object_id, extra_context=extra_context)


class PageAdmin(admin.ModelAdmin):
    list_display = ['name', 'index', 'site', 'slug']
    list_filter = ['site']


admin.site.register(Site, SiteAdmin)
admin.site.register(Page, PageAdmin)