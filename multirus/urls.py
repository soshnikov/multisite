from django.conf.urls import patterns, include, url
from pages import views as pages_views

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'multirus.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^redactor/', include('redactor.urls')),

    url(r'^$', pages_views.index),
    url(r'^page/(.+)/$', pages_views.page),
)
