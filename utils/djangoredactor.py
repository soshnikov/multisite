# -*- coding: utf-8 -*-

"""
Расширение django-wysywig-redactor

Created: 05.06.14 13:56
Author: Ivan Soshnikov, e-mail: ivan@wtp.su
"""

import os
from redactor.handlers import SimpleUploader
from multirus import settings

class RedactorUploadHandler(SimpleUploader):

    def get_url(self):
        """
        Return url for file if he saved else None
        """
        if not hasattr(self, 'real_path'):
            return None
        else:
            return os.path.join(settings.REDACTOR_UPLOAD_URL, os.path.basename(self.real_path))
